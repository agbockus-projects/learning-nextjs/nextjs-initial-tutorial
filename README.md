# Nextjs Initial Tutorial

## Description

This is the completed initial tutorial on the [Nextjs website](https://nextjs.org/learn).

## Requirements

- `node 20`
- `npm 10`

## Instructions

1. Run `git clone https://gitlab.com/agbockus-projects/learning-nextjs/nextjs-initial-tutorial.git` to clone down the repo.
2. `cd` into the folder and run `npm install`.
3. Run `npm run dev` to launch the application and then click on the localhost link to open the application.
